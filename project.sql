-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2017 at 08:41 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `qid` text NOT NULL,
  `ansid` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`qid`, `ansid`) VALUES
('55892169bf6a7', '55892169d2efc'),
('5589216a3646e', '5589216a48722'),
('558922117fcef', '5589221195248'),
('55892211e44d5', '55892211f1fa7'),
('558922894c453', '558922895ea0a'),
('558922899ccaa', '55892289aa7cf'),
('558923538f48d', '558923539a46c'),
('55892353f05c4', '55892354051be'),
('558973f4389ac', '558973f462e61'),
('558973f4c46f2', '558973f4d4abe'),
('558973f51600d', '558973f526fc5'),
('558973f55d269', '558973f57af07'),
('558973f5abb1a', '558973f5e764a'),
('5589751a63091', '5589751a81bf4'),
('5589751ad32b8', '5589751adbdbd'),
('5589751b304ef', '5589751b3b04d'),
('5589751b749c9', '5589751b9a98c');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `email` varchar(50) NOT NULL,
  `eid` text NOT NULL,
  `score` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `sahi` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`email`, `eid`, `score`, `level`, `sahi`, `wrong`, `date`) VALUES
('sunnygkp10@gmail.com', '558921841f1ec', 4, 2, 2, 0, '2015-06-23 09:31:26'),
('sunnygkp10@gmail.com', '558920ff906b8', 4, 2, 2, 0, '2015-06-23 13:32:09'),
('avantika420@gmail.com', '558921841f1ec', 4, 2, 2, 0, '2015-06-23 14:33:04'),
('avantika420@gmail.com', '5589222f16b93', 4, 2, 2, 0, '2015-06-23 14:49:39'),
('mi5@hollywood.com', '5589222f16b93', 4, 2, 2, 0, '2015-06-23 15:12:56'),
('nik1@gmail.com', '558921841f1ec', 1, 2, 1, 1, '2015-06-23 16:11:50'),
('sunnygkp10@gmail.com', '5589222f16b93', 1, 2, 1, 1, '2015-06-24 03:22:38'),
('sunnygkp10@gmail.com', '5589741f9ed52', -5, 5, 0, 5, '2017-08-26 00:00:39'),
('ahad@gmail.com', '5589741f9ed52', -2, 5, 1, 4, '2017-08-26 10:03:14'),
('s@gmail.com', '5589741f9ed52', -2, 2, 0, 2, '2017-08-26 18:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `qid` varchar(50) NOT NULL,
  `option` varchar(200) NOT NULL,
  `optionid` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`qid`, `option`, `optionid`) VALUES
('558973f51600d', ' Boot Record', '558973f526fb9'),
('558973f51600d', ' Boot Strapping', '558973f526fc5'),
('558973f51600d', ' Booting', '558973f526fce'),
('558973f4389ac', ' Contains only scripts to be executed during bootup', '558973f462e56'),
('558973f4389ac', ' Contains root-file system and drivers required to be preloaded during bootup', '558973f462e61'),
('558973f5abb1a', ' Csh', '558973f5e7636'),
('558973f55d269', ' Fast boot', '558973f57af27'),
('558973f55d269', ' Hot boot', '558973f57af17'),
('558973f5abb1a', ' ksh', '558973f5e7640'),
('558973f55d269', ' Quick boot', '558973f57aef1'),
('558973f5abb1a', ' sh', '558973f5e764a'),
('55892211e44d5', '$a', '55892211f1fa7'),
('5589751ad32b8', '0', '5589751adbdce'),
('55892353f05c4', '10.0.0.0.1', '55892354051b4'),
('5589751b304ef', '100', '5589751b3b05e'),
('55892353f05c4', '11.11.11.11', '55892354051be'),
('5589751b304ef', '111', '5589751b3b04d'),
('55892353f05c4', '172.168.16.2', '55892354051a3'),
('55892353f05c4', '192.168.1.100', '5589235405192'),
('5589751ad32b8', '23', '5589751adbdbd'),
('558923538f48d', '255.0.0.0', '558923539a46c'),
('558923538f48d', '255.255.0.0', '558923539a48b'),
('558923538f48d', '255.255.255.0', '558923539a480'),
('5589751bd02ec', '3', '5589751bdadaa'),
('5589751b749c9', '31', '5589751b9a9b7'),
('5589751b304ef', '345', '5589751b3b069'),
('5589751b749c9', '35', '5589751b9a9c9'),
('5589751a63091', '40', '5589751a81bfd'),
('5589751ad32b8', '54', '5589751adbdd8'),
('5589751b304ef', '543', '5589751b3b073'),
('5589751a63091', '56', '5589751a81bf4'),
('5589751ad32b8', '61', '5589751adbde2'),
('5589751bd02ec', '7', '5589751bdadaa'),
('5589751b749c9', '71', '5589751b9a9a5'),
('5589216a3646e', '751', '5589216a48713'),
('5589216a3646e', '752', '5589216a4871a'),
('5589216a3646e', '754', '5589216a4871f'),
('5589216a3646e', '755', '5589216a48722'),
('5589751b749c9', '78', '5589751b9a98c'),
('5589751a63091', '80', '5589751a81be8'),
('5589751bd02ec', '9', '5589751bdadaa'),
('5589751a63091', '99', '5589751a81bd6'),
('5589751bd02ec', 'autoindentation is not possible in vi editor', '5589751bdadaa'),
('558973f5abb1a', 'bash', '558973f5e7623'),
('558973f51600d', 'Boot Loading', '558973f526f9d'),
('558922899ccaa', 'cin', '55892289aa7df'),
('558922894c453', 'cin<<a;', '558922895ea26'),
('558922894c453', 'cin>>a;', '558922895ea0a'),
('558973f55d269', 'Cold boot', '558973f57af07'),
('558973f4c46f2', 'Commands', '558973f4d4ad9'),
('558973f4389ac', 'containing root file-system required during bootup', '558973f462e44'),
('558922894c453', 'couet>>a;', '558922895ea34'),
('558922117fcef', 'cout', '5589221195270'),
('558922899ccaa', 'coute', '55892289aa7cf'),
('558922894c453', 'couts<a;', '558922895ea41'),
('558922117fcef', 'echo', '5589221195248'),
('55892169bf6a7', 'groupmod', '55892169d2f0c'),
('55892211e44d5', 'int a', '55892211f1f97'),
('55892211e44d5', 'int a$', '55892211f1fbd'),
('558973f4c46f2', 'Kernel', '558973f4d4abe'),
('55892211e44d5', 'long int a', '55892211f1fb4'),
('558973f4389ac', 'None of the above', '558973f462e6b'),
('558923538f48d', 'none of these', '558923539a495'),
('558922117fcef', 'printa', '558922119525a'),
('558922117fcef', 'printfb', '5589221195265'),
('558922899ccaa', 'printfk', '55892289aa7f5'),
('558922899ccaa', 'printl', '55892289aa7eb'),
('558973f4c46f2', 'Script', '558973f4d4ae3'),
('558973f4c46f2', 'Shell', '558973f4d4acf'),
('55892169bf6a7', 'useradd', '55892169d2f05'),
('55892169bf6a7', 'useralter', '55892169d2f09'),
('55892169bf6a7', 'usermod', '55892169d2efc');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `eid` text NOT NULL,
  `qid` text NOT NULL,
  `qns` varchar(200) NOT NULL,
  `choice` int(10) NOT NULL,
  `sn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`eid`, `qid`, `qns`, `choice`, `sn`) VALUES
('5589741f9ed52', '5589751ad32b8', ' 19 + ……. = 42?', 4, 2),
('5589741f9ed52', '5589751bd02ec', ' 20 is divisible by ……… ', 4, 5),
('55897338a6659', '558973f55d269', ' Bootstrapping is also known as', 4, 4),
('5589741f9ed52', '5589751b749c9', ' How much is 90 – 19?', 4, 4),
('55897338a6659', '558973f51600d', ' The process of starting up a computer is known as', 4, 3),
('5589741f9ed52', '5589751a63091', ' What is the greatest two digit number?', 4, 1),
('5589741f9ed52', '5589751b304ef', ' What is the smallest three digit number?', 4, 3),
('55897338a6659', '558973f4389ac', 'On Linux, initrd is a file', 4, 1),
('55897338a6659', '558973f5abb1a', 'The shell used for Single user mode shell is:', 4, 5),
('55897338a6659', '558973f4c46f2', 'Which is loaded into memory when system is booted?', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `eid` text NOT NULL,
  `title` varchar(100) NOT NULL,
  `sahi` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `time` bigint(20) NOT NULL,
  `intro` text NOT NULL,
  `tag` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`eid`, `title`, `sahi`, `wrong`, `total`, `time`, `intro`, `tag`, `date`) VALUES
('55897338a6659', 'General Knowledge', 2, 1, 5, 10, '', 'General Knowledge', '2017-08-23 14:54:48'),
('5589741f9ed52', 'Simple math', 2, 1, 5, 10, '', 'Math', '2017-08-23 14:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `rank`
--

CREATE TABLE `rank` (
  `email` varchar(50) NOT NULL,
  `score` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rank`
--

INSERT INTO `rank` (`email`, `score`, `time`) VALUES
('sunnygkp10@gmail.com', 4, '2017-08-26 00:00:28'),
('avantika420@gmail.com', 8, '2015-06-23 14:49:39'),
('mi5@hollywood.com', 4, '2015-06-23 15:12:56'),
('nik1@gmail.com', 1, '2015-06-23 16:11:50'),
('s@gmail.com', 0, '2017-08-26 18:16:42'),
('ahad@gmail.com', -2, '2017-08-26 10:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `name` varchar(50) NOT NULL,
  `gender` varchar(5) NOT NULL,
  `college` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mob` bigint(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`name`, `gender`, `college`, `email`, `mob`, `password`) VALUES
('Ahad', 'M', 'Buet college', 'ahad@gmail.com', 1521220562, 'e10adc3949ba59abbe56e057f20f883e'),
('Shuvo', 'F', 'KNIT sultanpur', 's@gmail.com', 7785068889, 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD UNIQUE KEY `option` (`option`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD UNIQUE KEY `qns` (`qns`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
